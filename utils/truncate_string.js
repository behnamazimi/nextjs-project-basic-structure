function truncate(string) {
  if (string.length > 100)
    return string.substring(0, 100) + '...';
  else
    return string;
};
export default truncate