import cookie from "js-cookie";
import SimpleCrypto from "simple-crypto-js";

let simpleCrypto = new SimpleCrypto('jsdWEnmDFd3lsd0234SDF2D_sdf32smk__sd3fjhVsd2fbKxz9sdDdSDSSdfOd0fcsDFfSF');

export const setCookie = (key, value) => {


    if (process.browser) {
        cookie.set(key, simpleCrypto.encrypt(value), {
            expires: 10,
            path: "/"
        });
    }
};

export const removeCookie = key => {
    if (process.browser) {
        cookie.remove(key, {
            expires: 1
        });
    }
};

export const getCookie = (key, req) => {
    try {
        let data = process.browser
            ? getCookieFromBrowser(key)
            : getCookieFromServer(key, req);

        return data ? simpleCrypto.decrypt(data) : null;
    } catch (e) {
        return '';
    }
    return '';


};

const getCookieFromBrowser = key => {
    return cookie.get(key);
};

const getCookieFromServer = (key, req) => {
    if (!req.headers.cookie) {
        return undefined;
    }
    const rawCookie = req.headers.cookie
        .split(";")
        .find(c => c.trim().startsWith(`${key}=`));
    if (!rawCookie) {
        return undefined;
    }
    return rawCookie.split("=")[1];
};
