import Link from "next/link";

function paginationGenerator(pagesCount, currentPage, urlAs, urlHref) {
    let links = []

    if (!urlAs)
        urlAs = urlHref

    const limit = pagesCount < 3 ? pagesCount : 3
    const limitHalf = Math.floor(limit / 2)

    if (pagesCount <= limit) {
        for (let i = 1; i <= limit; i++) {
            let pageLink = <Link as={`/${urlAs}?page=${i}`} href={"/" + urlHref + "?page=" + i} key={i}>
                <a className={"pagination-item" + (currentPage === i ? ' current' : '')}>{i}</a>
            </Link>;
            links.push(pageLink)
        }

    } else if (currentPage - limitHalf <= limitHalf) {

        for (let i = 1; i <= limit; i++) {
            let pageLink = <Link as={`/${urlAs}?page=${i}`} href={"/" + urlHref + "?page=" + i} key={i}>
                <a className={"pagination-item" + (currentPage === i ? ' current' : '')}>{i}</a>
            </Link>;
            links.push(pageLink)
        }

        // adding more btn
        let dotsItem = <a className={"pagination-item more"} key={0.1}>...</a>;
        links.push(dotsItem);

        let lastPageLink = <Link as={`/${urlAs}?page=${pagesCount}`} href={"/" + urlHref + "?page=" + pagesCount}
                                 key={0.3}>
            <a className={"pagination-item" + (currentPage === pagesCount ? ' current' : '')}>{pagesCount}</a>
        </Link>;
        links.push(lastPageLink)

    } else if (currentPage - limitHalf > limitHalf) {

        // first
        let firstPageLink = <Link as={`/${urlAs}?page=${1}`} href={"/" + urlHref + "?page=" + 1} key={0.62}>
            <a className={"pagination-item" + (currentPage === 1 ? ' current' : '')}>{1}</a>
        </Link>;
        links.push(firstPageLink)

        // adding more btn
        let dotsItem = <a className={"pagination-item more"} key={0.95}>...</a>;
        links.push(dotsItem)


        if ((currentPage + limitHalf) < pagesCount - 1) {

            for (let i = currentPage - limitHalf; i <= currentPage + limitHalf; i++) {
                let pageLink = <Link as={`/${urlAs}?page=${i}`} href={"/" + urlHref + "?page=" + i} key={i}>
                    <a className={"pagination-item" + (currentPage === i ? ' current' : '')}>{i}</a>
                </Link>;

                links.push(pageLink)
            }

            // adding more btn
            let dotsItem = <a className={"pagination-item more"} key={0.99}>...</a>;
            links.push(dotsItem)

            let lastPageLink = <Link as={`/${urlAs}?page=${pagesCount}`}
                                     href={"/" + urlHref + "?page=" + pagesCount} key={0.3}>
                <a className={"pagination-item" + (currentPage === pagesCount ? ' current' : '')}>{pagesCount}</a>
            </Link>;
            links.push(lastPageLink)


        } else {
            for (let i = pagesCount - limit; i <= pagesCount; i++) {
                let pageLink = <Link as={`/${urlAs}?page=${i}`} href={"/" + urlHref + "?page=" + i} key={i}>
                    <a className={"pagination-item" + (currentPage === i ? ' current' : '')}>{i}</a>
                </Link>;
                links.push(pageLink)
            }
        }
    }

    return links
}


export default paginationGenerator