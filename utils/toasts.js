import {toast, Zoom} from 'react-toastify';

const Icon = ()=>{
    return (
        'You should import Icon instead of this'
    )
}

let options = {
    position: "bottom-center",
    autoClose: 4000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    transition: Zoom,
    rtl: true
};

export const toasts = {
    error: function (message, icon = '') {
        message = <span>{icon && <Icon name={icon}/>}{message}</span>;
        if (icon && icon !== '')
            options.className = (options.className || '') + ' with-icon';
        toast.error(message, options);
    },
    info: function (message, icon = '') {
        message = <span>{icon && <Icon name={icon}/>}{message}</span>;
        if (icon && icon !== '')
            options.className = (options.className || '') + ' with-icon';
        toast.info(message, options);
    },
    success: function (message, icon = '', opts) {
        // icon = null;
        message = <span>{icon && <Icon name={icon}/>}{message}</span>;
        options = Object.assign(options, opts);
        if (icon && icon !== '')
            options.className = (options.className || '') + ' with-icon';
        toast.success(message, options);
    },
    warning: function (message, icon = '') {
        message = <span>{icon && <Icon name={icon}/>}{message}</span>;
        if (icon && icon !== '')
            options.className = (options.className || '') + ' with-icon';
        toast.warning(message, options);
    },
};