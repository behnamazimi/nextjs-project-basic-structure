const process = require('process');
const express = require('express');
const next = require('next');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = !process.argv.includes('-p');

const app = next({dev});
const handle = app.getRequestHandler();

app.prepare()
    .then(() => {
        const server = express();

        server.get('/', (req, res) => {
            const actualPage = '/index';
            let queryParams = {
                page: req.query.page || req.params.page || 1
            };

            app.render(req, res, actualPage, queryParams)

        })

        server.get('/sitemap.xml', (req, res) => {

            let options = {
                root: __dirname + '/static/',
                headers: {
                    'Content-Type': 'text/plain;charset=UTF-8'
                }
            };

            res.status(200).sendFile('sitemap.xml', options)
        });

        server.get('/robots.txt', (req, res) => {

            let options = {
                root: __dirname + '/static/',
                headers: {
                    'Content-Type': 'text/plain;charset=UTF-8'
                }
            };

            res.status(200).sendFile('robots.txt', options)
        });

        server.get('/login', (req, res) => {

            const actualPage = '/login';
            let queryParams = {};

            if (req.params.friend)
                queryParams.friend = req.params.friend;

            app.render(req, res, actualPage, queryParams)
        });

        server.get('*', (req, res) => handle(req, res));

        server.listen(port)
    });

