import React from "react";
import Page from '../layouts/master'
import PropTypes from "prop-types"
import { appConstants } from "../constants/app.constants";

function IndexPage(props) {

    return (
        <Page title={"Index Page"} pageClass="page index-page">
            Index Page
        </Page>
    )
}

IndexPage.getInitialProps = (ctx) => {

    // const authToken = getJwtToken(ctx);
    // const is_logged_in = isAuthenticated(ctx);
    // let user = getUserDetailsFromCookie(appConstants.USER_COOKIE_KEY, ctx);
    // if (user)
    //     user = JSON.parse(decodeURIComponent(user));
    // else
    //     user = {};

    return {}
}

IndexPage.propTypes = {};

export default IndexPage;
