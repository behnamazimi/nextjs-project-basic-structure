import React from "react";
import ErrorPage from "../components/errors";

class ErrorHandling extends React.Component {

	constructor(props) {
		super(props);
	}

	static async getInitialProps(ctx) {

		const {res, err} = ctx;

		const statusCode = res ? res.statusCode : err ? err.statusCode : null;

		return {
			statusCode
		}
	}

	render() {
		const {statusCode} = this.props;

		return (
			<ErrorPage status={statusCode}/>
		)
	}
}

export default ErrorHandling;
