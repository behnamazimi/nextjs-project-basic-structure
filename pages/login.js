import React from "react";
import Page from '../layouts/master'
import PropTypes from "prop-types"

function LoginPage(props) {

    return (
        <Page title={"Login Page"} pageClass="page login-page">
            Login Page
        </Page>
    )
}

LoginPage.getInitialProps = (ctx) => {

    return {}
}

LoginPage.propTypes = {};

export default LoginPage;
