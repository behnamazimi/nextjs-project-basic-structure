import React from "react";
import {logout} from "../services/auth.service";
import {requestUserLogout} from "../actions/auth.actions";
import redirect from "../utils/redirect";

class LogoutPage extends React.Component {

    static getInitialProps(ctx) {
        logout(ctx);

        ctx.reduxStore.dispatch(requestUserLogout());

        redirect('/', ctx)
        return {};
    }

    render() {
        return (
            <p className="text-center inform-text" style={{marginTop: "5vh"}}>لطفا کمی منتظر باشید...</p>
        );
    }

}

export default LogoutPage;
