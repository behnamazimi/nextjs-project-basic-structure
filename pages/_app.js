import App, {Container} from 'next/app'
import React from 'react'
import withReduxStore from '../utils/with_redux_store'
import {Provider} from 'react-redux'
import {appConstants} from "../constants/app.constants";
import {getJwtToken, getUserDetailsFromCookie, isAuthenticated} from "../services/auth.service";
import {setAuthData} from "../actions/auth.actions";
import {setRedirectURL} from "../actions/settings.action";
import {getCookie} from "../utils/session";

class MyApp extends App {

    static async getInitialProps({Component, router, ctx}) {

        let pageProps = await Component.getInitialProps(ctx);

        const {reduxStore} = ctx;
        const isLoggedIn = isAuthenticated(ctx);
        const token = getJwtToken(ctx);
        let user = getUserDetailsFromCookie(appConstants.USER_COOKIE_KEY, ctx);
        if (user)
            user = JSON.parse(decodeURIComponent(user));
        else
            user = null;

        // set basic auth data
        reduxStore.dispatch(setAuthData(isLoggedIn, user, token));

        //set redirectURL
        const redirectURL = getCookie('redirectURL', ctx.req)
        if (redirectURL)
            reduxStore.dispatch(setRedirectURL(redirectURL));

        return {
            pageProps,
        }
    }

    render() {
        const {Component, pageProps, reduxStore} = this.props;

        return (
            <Container>
                <Provider store={reduxStore}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        )
    }
}

export default withReduxStore(MyApp)