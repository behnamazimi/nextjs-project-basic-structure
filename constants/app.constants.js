export const appConstants = {
    BASE_URL: 'http://localhost:3333/api/v1',
    FILES_URL: 'http://localhost:3333/',

    _BASE_URL: 'http://example.com/api/v1',
    _FILES_URL: 'http://example.com/',

    APP_NAME: 'Your Project Name',
    APP_URL: 'http://example.com',
    APP_VERSION: '0.1',

    APP_DESC_META: 'You SEO friendly description here',
    APP_KEYWORDS_META: 'You SEO friendly keywords here',
    APP_OGIMAGE: '',

    // auth
    USER_COOKIE_KEY: '__le_d0',
    TOKEN_COOKIE_KEY: '_ew_kjz5',

};