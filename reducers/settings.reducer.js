// REDUCERS
import {settingsActionTypes} from "../constants/redux.constants";

const initialState = {
    navbarOpen: false,
    redirectURL: '',
    pageReqs: {},
    windowDim: {}
};

const settingsReducer = (state = initialState, action) => {

    switch (action.type) {

        case settingsActionTypes.TOGGLE_MAIN_NAVBAR:

            let newStatus = !state.navbarOpen

            if (action.payload.status !== undefined && action.payload.status !== null)
                newStatus = action.payload.status
            return {
                ...state,
                isAuthenticated: action.payload.isAuthenticated,
                navbarOpen: newStatus
            };

            break;

        case settingsActionTypes.SET_REDIRECT_URL:
            return {
                ...state,
                redirectURL: action.payload.URL
            };

        case settingsActionTypes.SET_PAGE_REQUIREMENTS:
            return {
                ...state,
                pageReqs: action.payload.pageReqs
            };

            break;

        case settingsActionTypes.SET_WINDOW_DIM:

            let screen = ''

            if (action.payload.w < 575)
                screen = 'sm'
            else if (action.payload.w < 768)
                screen = 'md'
            else if (action.payload.w < 991)
                screen = 'lg'
            else if (action.payload.w > 991)
                screen = 'xl'

            return {
                ...state,
                windowDim: {
                    w: action.payload.w,
                    h: action.payload.h,
                    screen
                },
            };

            break;

        default:
            return state
    }
};

export default settingsReducer