import {combineReducers} from "redux";
import authReducer from "./auth.reducers";
import settingsReducer from "./settings.reducer";

const rootReducer = combineReducers({
    auth: authReducer,
    settings: settingsReducer
});

export default rootReducer;