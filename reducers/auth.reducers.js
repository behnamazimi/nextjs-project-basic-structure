// REDUCERS
import {setCookie, getCookie} from "../utils/session";
import {appConstants} from "../constants/app.constants";
import {authActionTypes} from "../constants/redux.constants";

const initialState = {
    isAuthenticated: false,
    authToken: null,
    authUser: null,
    firstLogin: false
};


const authReducer = (state = initialState, action) => {

    switch (action.type) {

        case authActionTypes.SET_AUTH_DATA:

            return {
                ...state,
                isAuthenticated: action.payload.isAuthenticated,
                authToken: action.payload.token,
                authUser: action.payload.user
            };

        case authActionTypes.REQUEST_USER_LOGIN:
            setCookie(appConstants.TOKEN_COOKIE_KEY, action.payload.token);
            setCookie(appConstants.USER_COOKIE_KEY, action.payload.user);
            return {
                ...state,
                isAuthenticated: true,
                authToken: action.payload.token,
                authUser: action.payload.user
            };

        case authActionTypes.REQUEST_USER_LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                authToken: null,
                authUser: null,
                firstLogin: false
            };

        case authActionTypes.SET_FIRST_LOGIN_FLAG:
            return {
                ...state,
                firstLogin: action.payload.first_login
            };


        case authActionTypes.REQUEST_USER_UPDATE:
            setCookie(appConstants.USER_COOKIE_KEY, action.payload.user);
            return {
                ...state,
                authUser: action.payload.user
            };

        default:
            return state
    }
};

export default authReducer