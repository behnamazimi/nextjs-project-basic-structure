import * as axios from "axios";
import {appConstants} from "../constants/app.constants";
import redirect from "../utils/redirect";
import {setCookie, getCookie, removeCookie} from "../utils/session";

export const userLogin = (email, password) => {
    // do login process here!!!
    const credentials = {
        email,
        password,
        web: true
    };

    return axios({
        method: 'post',
        url: appConstants.BASE_URL + "/auth/users/login",
        headers: {
            'X-Auth-Type': 'user',
        },
        data: credentials
    })

};

export const sendSmsToken = (phone, isPasswordReset = false) => {

    let data = {
        phone,
    };

    if (isPasswordReset)
        data.isPasswordReset = true

    return axios({
        method: 'post',
        url: appConstants.BASE_URL + "/auth/send-token",
        headers: {
            'X-Auth-Type': 'user',
        },
        data: data
    })

};

export const resetPassword = (data) => {

    return axios({
        method: 'post',
        url: appConstants.BASE_URL + "/auth/reset-password",
        headers: {
            'X-Auth-Type': 'user',
        },
        data
    })

};

export const checkSmsToken = (phone, token, hash_key) => {

    const data = {
        phone,
        token,
        hash_key
    };

    return axios({
        method: 'post',
        url: appConstants.BASE_URL + "/verify/check-token",
        headers: {
            'X-Auth-Type': 'user',
        },
        data: data
    })

};

export const userRegister = async (data = {}) => {

    data.web = true

    return axios({
        method: 'post',
        url: appConstants.BASE_URL + "/auth/users/register",
        headers: {
            'X-Auth-Type': 'user',
        },
        data
    })
};

export const logout = (ctx = {}) => {
    if (process.browser) {
        removeCookie(appConstants.USER_COOKIE_KEY);
        removeCookie(appConstants.TOKEN_COOKIE_KEY);
        redirect("/", ctx);
    }
};

export const getJwtToken = ctx => {
    return getCookie(appConstants.TOKEN_COOKIE_KEY, ctx.req);
};

export const getUserDetailsFromCookie = (key, ctx) => {
    return getCookie(key, ctx.req);
};

export const isAuthenticated = ctx => {
    let user = getUserDetailsFromCookie(appConstants.USER_COOKIE_KEY, ctx);
    if (IsJsonString(user))
        user = JSON.parse(user);

    let token = getJwtToken(ctx);
    return !!token && !!user && !!user.id;
};

export const redirectIfAuthenticated = ctx => {
    if (isAuthenticated(ctx)) {
        redirect("/", ctx);
        return true;
    }
    return false;
};

export const redirectIfNotAuthenticated = ctx => {
    if (!isAuthenticated(ctx)) {
        redirect("/login", ctx);
        return true;
    }
    return false;
};

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
