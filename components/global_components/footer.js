import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

const Footer = (props) => {

    return (
        <footer className="main-footer">
            Footer
        </footer>
    )
};

Footer.propTypes = {
};

Footer.defaultProps = {
};

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(Footer);