import Link from "next/link";
import {withRouter} from "next/router";
import {Children} from "react";
import React from "react";
import cs from "classnames"

function ActiveLink({router, children, as, href, activeClassName, ...rest}) {

    return (
        <Link {...rest} href={href} as={as}>
            {React.cloneElement(Children.only(children), {
                className: cs(
                    children.props.className,
                    (router.asPath === href || router.asPath === as) ? (activeClassName || 'active') : null
                )
            })}
        </Link>
    )
}

export default withRouter(ActiveLink)