import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

/**
 with screen HOC
 **/

export const withScreen = (Component) => {

    class ResponsiveHOC extends Component {

        constructor(props) {
            super(props)
        }

        render() {
            return <Component {...this.props} {...this.props.windowDim}/>
        }
    }

    function mapStateToProps(state) {
        return {
            windowDim: state.settings.windowDim
        }
    }

    return connect(mapStateToProps)(ResponsiveHOC)
}

/**
 * @return {null, XML}
 */
function Responsive({children, windowDim, minDeviceWidth, maxDeviceWidth}) {

    if (minDeviceWidth && windowDim.w < minDeviceWidth)
        return null

    if (maxDeviceWidth && windowDim.w > maxDeviceWidth)
        return null

    return (
        <React.Fragment>
            {children}
        </React.Fragment>
    );

}

Responsive.propTypes = {
    minDeviceWidth: PropTypes.number.isRequired,
    maxDeviceWidth: PropTypes.number.isRequired,
}

Responsive.defaultProps = {}

function mapStateToProps(state) {
    return {
        windowDim: state.settings.windowDim
    }
}

export default connect(mapStateToProps)(Responsive)
