import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";

function FlexMasonry({columns, gap, responsive, children}) {

    let currentColumns = columns
    let currentGap = gap

    const [windowWidth, setWindowWidth] = useState(5000)

    useEffect(() => {
        setWindowWidth(window.innerWidth)
    })

    useEffect(() => {
        window.addEventListener('resize', (e) => setWindowWidth(e.target.innerWidth))

        return (
            window.removeEventListener('resize', (e) => setWindowWidth(e.target.innerWidth))
        )
    })

    if (responsive) {

        Object.keys(responsive).sort().reverse().forEach((size) => {

            if (windowWidth < size) {
                currentColumns = responsive[size].columns || columns
                currentGap = responsive[size].gap || gap
            }
        })
    }

    const columnWrapper = {};
    const result = [];

    // create columns
    for (let i = 0; i < currentColumns; i++) {
        columnWrapper[`column_${i}`] = [];
    }

    // push child in each column
    for (let i = 0; i < children.length; i++) {
        const columnIndex = i % currentColumns;

        columnWrapper[`column_${columnIndex}`].push(
            <div style={{marginBottom: `${currentGap}px`}} key={'child' + i}>
                {children[i]}
            </div>
        );

    }

    // push columns in result
    for (let i = 0; i < currentColumns; i++) {

        const marginLeft = i < currentColumns - 1 ? currentGap : 0

        result.push(
            <div className={`column-${i}`} key={"col-" + i}
                 style={{
                     marginLeft: `${marginLeft}px`,
                     flex: 1,
                     width: `${(100 / currentColumns) - currentGap}%`,
                     transition: '.3s',
                 }}>
                {columnWrapper[`column_${i}`]}
            </div>
        );

    }


    return (
        <div style={{
            display: 'flex',
            transition: '.3s',
        }}>
            {result}
        </div>
    );

}

FlexMasonry.propTypes = {
    columns: PropTypes.number.isRequired,
    gap: PropTypes.number.isRequired,
    responsive: PropTypes.object,
    children: PropTypes.arrayOf(PropTypes.element),
}

FlexMasonry.defaultProps = {
    columns: 2,
    gap: 20,
}

export default FlexMasonry