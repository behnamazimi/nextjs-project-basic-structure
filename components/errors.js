import React from "react";
import Page from '../layouts/master'
import { getJwtToken, getUserDetailsFromCookie, isAuthenticated } from "../services/auth.service";
import PropTypes from "prop-types"
import { appConstants } from "../constants/app.constants";

function ErrorPage(props) {
    let status_text = 'Unhandeled error!';

    if (props.status === 404)
        status_text = 'Page not found!';

    return (
        <Page title={"Error " + (props.status || 500)} pageClass="page error-page"
            auth={props.auth} user={props.user}>

            <div className="container-fluid mx-auto">
                <div className="card">
                    <h3 className="status-code">
                        {props.status || 500}
                    </h3>
                    <p className="none-content" style={{
                        textAlign: 'center',
                        fontSize: 'calc(1vh + 1vw + 0.2vmin)',
                        lineHeight: 'calc(1vh + 1vw + 0.2vmin)'
                    }}>{props.message || status_text}</p>
                    <p style={{
                        color: '#999',
                        textAlign: 'center',
                        fontSize: 'calc(1vh + .1vw + 0.2vmin)',
                        lineHeight: 'calc(1vh + .1vw + 0.2vmin)'
                    }}>{props.error}</p>
                </div>
            </div>
        </Page>
    )
}

ErrorPage.getInitialProps = (ctx) => {

    const token = getJwtToken(ctx);
    const is_logged_in = isAuthenticated(ctx);
    let user = getUserDetailsFromCookie(appConstants.USER_COOKIE_KEY, ctx);
    if (user)
        user = JSON.parse(decodeURIComponent(user));
    else
        user = {};

    return {
        token,
        user,
        is_logged_in,
    }
}

ErrorPage.propTypes = {
    status: PropTypes.number.isRequired,
    message: PropTypes.string,
    error: PropTypes.string,
};

export default ErrorPage;
