import Header from "../components/global_components/header"
import Footer from "../components/global_components/footer"
import Head from "../components/global_components/head"
import { ToastContainer } from 'react-toastify'
import PropTypes from "prop-types"
import NProgress from 'nprogress'
import Router from 'next/router'
import React from "react"
import "../styles/index.scss"
import { connect } from "react-redux";
import { Scrollbars } from "react-custom-scrollbars";
import cs from "classnames";
import { setWindowDim } from "../actions/settings.action";

Router.onRouteChangeStart = (url) => NProgress.start()
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()


class Page extends React.Component {

    constructor(props) {
        super(props)

        this.state = {}

    }

    componentDidMount() {

        // set listener and store dim in redux
        this.props.dispatch(setWindowDim(window.innerWidth, window.innerHeight))

        window.addEventListener('resize', () => {
            this.props.dispatch(setWindowDim(window.innerWidth, window.innerHeight))
        })

    }

    componentWillUnmount() {

        // remove resize listener
        window.removeEventListener('resize', () => {
            this.props.dispatch(setWindowDim(window.innerWidth, window.innerHeight))
        })

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            listenScroll: nextProps.listenScroll
        })
    }

    componentWillUnmount() {
        if (this.state.pusher)
            this.state.pusher.disconnect();
    }

    handleScrollEmit = (data) => {

        const st = data.scrollTop + 10;
        const sh = data.scrollHeight - data.clientHeight;

        this.setState({
            scrollDetails: {
                st,
                sh
            }
        })

        // check scroll listen status and emit page scroll value if it's true
        if (this.props.listenScroll && st > sh)
            _.invoke(this.props, 'emitScroll', st, sh)
    }

    render() {
        const {
            children, title, pageClass, description, w,
        } = this.props

        return (
            <React.Fragment>
                <Scrollbars
                    autoHide
                    className="main-rtl-scrollbar"
                    onUpdate={this.handleScrollEmit}
                    autoHideTimeout={300}
                    autoHideDuration={200}
                    style={{ width: '100%', minHeight: '100vh' }}>
                    <Head title={title} description={description} />
                    <Header w={w} />
                    <main className={cs(pageClass, 'pr')}>
                        {children}
                    </main>
                    <Footer />

                </Scrollbars>
                <ToastContainer />
            </React.Fragment>
        )
    }

}

Page.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    pageClass: PropTypes.string,
    listenScroll: PropTypes.bool,
    emitScroll: PropTypes.func,
}

Page.defaultProps = {
    title: '',
    description: '',
    pageClass: '',
}

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(Page);
