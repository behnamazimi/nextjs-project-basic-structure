// AUTH ACTIONS
import {settingsActionTypes} from "../constants/redux.constants";
import {setCookie} from "../utils/session";

export const setWindowDim = (w, h) => dispatch => {

    return dispatch({
        type: settingsActionTypes.SET_WINDOW_DIM,
        payload: {
            h, w
        }
    })
};

export const toggleMainNavbar = (status = null) => dispatch => {

    return dispatch({
        type: settingsActionTypes.TOGGLE_MAIN_NAVBAR,
        payload: {
            status
        }
    })
};

export const setRedirectURL = (URL = '') => dispatch => {

    setCookie('redirectURL', URL)
    return dispatch({
        type: settingsActionTypes.SET_REDIRECT_URL,
        payload: {
            URL
        }
    })
};

export const setPageRequirements = (pageReqs = {}) => dispatch => {

    return dispatch({
        type: settingsActionTypes.SET_PAGE_REQUIREMENTS,
        payload: {
            pageReqs
        }
    })
};
