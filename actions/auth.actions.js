// AUTH ACTIONS
import {authActionTypes} from "../constants/redux.constants";

export const requestUserLogin = (user, token) => dispatch => {

    return dispatch({
        type: authActionTypes.REQUEST_USER_LOGIN,
        payload: {
            user,
            token
        }
    })
};

export const requestUserUpdate = (user) => dispatch => {

    return dispatch({
        type: authActionTypes.REQUEST_USER_UPDATE,
        payload: {
            user,
        }
    })
};

export const requestUserLogout = () => dispatch => {

    return dispatch({
        type: authActionTypes.REQUEST_USER_LOGOUT,
    })
};

export const setFirstLoginFlag = (first_login) => dispatch => {

    return dispatch({
        type: authActionTypes.SET_FIRST_LOGIN_FLAG,
        payload:{
            first_login
        }
    })
};


export const setAuthData = (isAuthenticated, user, token) => dispatch => {

    return dispatch({
        type: authActionTypes.SET_AUTH_DATA,
        payload: {
            isAuthenticated,
            user,
            token
        }
    })
};

