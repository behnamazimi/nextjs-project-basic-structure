# Basic Project with NextJS

Bacis web project needs, utils and configs by [NextJS 7](https://nextjs.org)

Some of the packages and libraries that we will use are:
* `redux` - To handle app state
* `express` - To run app on it
* `axios` - To fetch data
* `js-cookie` - To handle local and server-side cookies
* `nprogress` - To show a cool loading on route chane
* `react-toastify` - To show managed toast messages/alerts
* `symple-crypto-js` - To encrypt/decrypt important data

### Styles
To code styles you should see `/styles` directory. There are `css` and `scss` support. Only the `/styles/index.scss` imported in the **master layout** and the other style files imported in the `index.scss`.

### Analyze Project
To analyze the whole project and bundle.js, just run `npm run analyze`. It use **webpack-bundle-analyzer** to analyze our output.

### Some Utils
There are some **utils** in directory with the same name to use in project where you need.

### Some Tips
* Make your own changes in `/constants/app.constants.js` file
* Edit `/components/global_components/head` as you wish
* To handle and use **Icons** we use a seperate component with same name, but there are not any added styles or font-faces for icons and its just a simple component. To have an icon set in your project, you could use **[icomoon.io](https://icomoon.io)** features and make your own icon set. Or you could use any other icon set in this project
* We added a seperate file for each redux state handler, for example we have `/constants/redux.constants.js` to keep action types and `/reducers/auth.reducer.js` to handle auth reducer switch and also `/actions/auth.actions.js` to handle all auth actions. Add other reducers,actions, and action types as you need.
* We put all XHR and core related files in the `/services` directory. Feel free to add new one to handle your data fetch.
